<?php

use Illuminate\Database\Seeder;

// Extensiones
use App\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Role::truncate();
        User::truncate();
        Permission::truncate();

        // Llenar la tabla Role
        $adminRole = Role::create(['name' => 'Admin']);
        $writerRole = Role::create(['name' => 'Writer']);


        $admin= new User();
        $admin->name = 'Geancarlos Chamorro Espinoza';
        $admin->email = 'geancarlo.chamorro@anddes.com';
        $admin->save();

        $writer = new User();
        $writer->name = 'GCE';
        $writer->email = 'geancarlosce96@gmail.com';
        $writer->save();

        // Tabla mode_has_roles
        $admin->assignRole($adminRole);
        $writer->assignRole($writerRole);




    }
}
