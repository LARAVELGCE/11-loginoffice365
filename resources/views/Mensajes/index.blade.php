@extends('layouts.app')

@section('content')
    <table class="table table-sm ">
        <thead class="table-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col">Mensaje</th>
            <th scope="col">Acciones</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
        </tr>
        {{--        @forelse ($dataMensajes as $key => $value)
                    <tr>
                        <th scope="row">{{ $key }}</th>
                        <td><a href="{{route('mensajes.show',$value->id) }}">{{ $value->nombre }}</a></td>
                        <td>{{ $value->email }}</td>
                        <td>{{ $value->mensaje }}</td>
                        <td>
                            <a href="{{ route('mensajes.edit',$value->id) }}">Editar</a>
                            <form method="post" action="{{ route('mensajes.destroy',$value->id) }}"  style="display: inline">
                            {{ csrf_field() }}
                            <!-- Metodo de eliminacion -->
                                {{ method_field('DELETE') }}
                                <button type="submit">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>No hay data</td>
                    </tr>
                @endforelse--}}
        </tbody>
    </table>
@endsection
