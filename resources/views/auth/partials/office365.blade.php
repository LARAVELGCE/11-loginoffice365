<div class="col-md-4">
    <div class="card ">
        <div class="card-header text-white bg-primary">{{ __("Socialite") }}</div>
        <div class="card-body">
            <a
                    href="{{ route('social_auth', ['driver' => 'graph']) }}"
                    class="btn btn-outline-primary btn-lg btn-block"
            >
                {{ __("Office 365") }} <i class="fa fa-envelope"></i>
            </a>

            <a
                    href="{{ route('social_auth', ['driver' => 'facebook']) }}"
                    class="btn btn-primary btn-lg btn-block"
            >
                {{ __("Facebook") }} <i class="fa fa-facebook"></i>
            </a>

        </div>
    </div>
</div>
